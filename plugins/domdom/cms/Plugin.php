<?php namespace Domdom\Cms;

use Backend;
use System\Classes\PluginBase;
use BackendMenu;
/**
 * cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'cms',
            'description' => 'No description provided yet...',
            'author'      => 'domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Domdom.Cms', 'cms', 'plugins/domdom/cms/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
            'Domdom\Cms\Components\CpHome' => 'cphome',
            'Domdom\Cms\Components\CpPosts' => 'cpposts',
            'Domdom\Cms\Components\Cpabout' => 'cpabout',
            'Domdom\Cms\Components\CpSinglePost' => 'cpsinglepost',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.cms.some_permission' => [
                'tab' => 'cms',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'CMS Custom',
                'url'         => Backend::url('domdom/cms/home/update/1'),
                'icon'        => 'icon-connectdevelop',
                'permissions' => ['domdom.cms.*'],
                'order'       => 500,

                'sideMenu' => [
                    'homepage' => [
                        'label' => 'Homepage',
                        'url'   => Backend::url('domdom/cms/home/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],
                    'aboutpage' => [
                        'label' => 'About page',
                        'url'   => Backend::url('domdom/cms/about/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],
                    'posts' => [
                        'label' => 'Posts',
                        'url'   => Backend::url('domdom/cms/posts'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'posts'
                    ],
                    'generaloption' =>[
                        'label' => 'General Options',
                        'url'   => Backend::url('domdom/cms/generaloption'),
                        'icon'        => 'icon-glide-g',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'General Options'

                    ],

                ]
            ],
        ];
    }
}

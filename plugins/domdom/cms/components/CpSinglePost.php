<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;
use Domdom\Cms\Models\Posts;
use Domdom\Cms\Models\Comment;
use Domdom\Cms\Models\Reply;


class CpSinglePost extends ComponentBase
{
    public $singlePost;
    public $postID;
    public $debug;
    public $commentDb;
    public $replyDB;
    public $currentCommentId;
    public $currentPostId;
    public $commentId;

    public function componentDetails()
    {
        return [
            'name'        => 'CpSinglePost Component',
            'description' => 'No description provided yet...'
        ];
    }
    function onRun()
    {
        parent::onRun(); // TODO: Change the autogenerated stub

        $this->commentDb = Comment::all();
        $this->singlePost = Posts::where( 'slug', $this->property('slug') )->first();
        $this->currentPostId = Posts::where( 'slug', $this->property('slug') )->first();
        $this->replyDB = Reply::all();

    }
    public function defineProperties()
    {
        return [];
    }
    public function getAllComments()
    {
        $this->commentDb = Comment::all();
    }
    public function getReplyById( $id ){

        $this->replyDB = Reply::all();

    }
    public function getcurrentCommentId( $id ){

        $this->currentCommentId = $id;

    }
    public function setCommentId($id){
        $this->commentId = $id;
    }
    public function refreshId() {
        $this->currentPostId = Posts::where( 'slug', $this->property('slug') )->first();
        $this->commentDb = Comment::all();

    }
    public function onAddComment(){
        $data = post();
        $comment = $data['comment'];
        $commentDb = new Comment();
        $commentDb->content = $comment;
        $commentDb->postId = Posts::where( 'slug', $this->property('slug') )->first()->id;
        $commentDb->userId = '1';
        if( $commentDb->content){
            $commentDb->save();
        }
        $this->getAllComments();
        $this->getReplyById( $this->commentId );
        $this->refreshId();

    }

    public function onAddReplyComment() {
        $data = post();

        $replyComment = $data['replyComment'];
        $parentComment = $data['parentCommentId'];

        $replyDb = new Reply();
        $replyDb->commentId = $parentComment;
        $replyDb->userId = '1';
        $replyDb->text = $replyComment;
        if($replyDb->text){
            $replyDb->save();
        }

        $this->setCommentId($replyDb->commentId);
        $this->getAllComments();
        $this->getReplyById( $replyDb->commentId );


        $this->refreshId();

    }
}

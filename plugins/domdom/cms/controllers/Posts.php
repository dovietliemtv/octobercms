<?php namespace Domdom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Posts Back-end Controller
 */
class Posts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Domdom.Cms', 'cms', 'posts');
    }
    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image' ) {
            return '<img src="' . $record->featured_image->path . '" width="50"/>';
        }
    }
}

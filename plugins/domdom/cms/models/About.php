<?php namespace Domdom\Cms\Models;

use Model;

/**
 * about Model
 */
class About extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_abouts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];
    protected $jsonable = ['content'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [

    ];
    public $attachOne = [
        'content_image' => 'System\Models\File'
    ];
    public $attachMany = [];
}

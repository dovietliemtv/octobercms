<?php namespace Domdom\Cms\Models;

use Model;

/**
 * GeneralOption Model
 */
class GeneralOption extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_general_options';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['social'];
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'logo'  =>  'System\Models\File'
    ];
    public $attachMany = [];
    public function getNameSocialOptions() {
        return [
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'youtube-play' => 'YouTube',
        ];
    }
}

<?php namespace Domdom\Cms\Models;

use Model;

/**
 * home Model
 */
class Home extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_homes';

    public $rules = [
        'imageContent4' => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'imageContent4' => 'System\Models\File',
        'imageContent5' => 'System\Models\File',
        'imageContent6' => 'System\Models\File',
        'people_image1' => 'System\Models\File',
        'people_image2' => 'System\Models\File',
        'people_image3' => 'System\Models\File',
    ];
    public $attachMany = [];
}

<?php namespace Domdom\Cms\Models;

use Model;

/**
 * Posts Model
 */
class Posts extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_posts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image' => 'System\Models\File'
    ];
    public $attachMany = [];

}

<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAboutsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_abouts')){
            Schema::create('domdom_cms_abouts', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('content');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_abouts');
    }
}

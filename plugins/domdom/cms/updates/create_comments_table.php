<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_comments')) {
            Schema::create('domdom_cms_comments', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('content');
                $table->text('postId');
                $table->text('userId');
                $table->timestamps();
            });
        }


    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_comments');
    }
}

<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGeneralOptionsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_general_options')){
            Schema::create('domdom_cms_general_options', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->json('social');
                $table->string('headtitle');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_general_options');
    }
}

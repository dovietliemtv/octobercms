<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_homes')){
            Schema::create('domdom_cms_homes', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title1');
                $table->text('content1');
                $table->text('title2');
                $table->text('content2');
                $table->text('title3');
                $table->text('content3');
                $table->text('title4');
                $table->text('content4');
                $table->text('title5');
                $table->text('content5');
                $table->text('title6');
                $table->text('content6');
                $table->text('peopleSayTitle');
                $table->text('peopleSayTitle1');
                $table->text('peopleSayContent1');
                $table->text('peopleSayTitle2');
                $table->text('peopleSayContent2');
                $table->text('peopleSayTitle3');
                $table->text('peopleSayContent3');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_homes');
    }
}

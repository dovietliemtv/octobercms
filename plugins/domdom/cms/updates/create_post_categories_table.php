<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostCategoriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_post_categories')){
            Schema::create('domdom_cms_post_categories', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('postId');
                $table->text('categoryId');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_post_categories');
    }
}

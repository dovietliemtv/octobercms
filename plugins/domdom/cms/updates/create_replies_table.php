<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRepliesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_replies')){
            Schema::create('domdom_cms_replies', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('commentId');
                $table->text('userId');
                $table->text('text');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_replies');
    }
}

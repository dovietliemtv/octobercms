<?php namespace Domdom\Membership;

use Backend;
use System\Classes\PluginBase;

/**
 * membership Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'membership',
            'description' => 'No description provided yet...',
            'author'      => 'domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
            'Domdom\Membership\Components\CpMember' => 'cpmember',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.membership.some_permission' => [
                'tab' => 'membership',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'membership' => [
                'label'       => 'membership',
                'url'         => Backend::url('domdom/membership/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.membership.*'],
                'order'       => 500,
            ],
        ];
    }
}

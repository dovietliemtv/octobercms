<?php namespace Domdom\Membership\Models;

use Model;

/**
 * Members Model
 */
class Members extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_membership_members';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}

<?php namespace Domdom\Signupemail;

use Backend;
use System\Classes\PluginBase;

/**
 * signupemail Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */

    public function pluginDetails()
    {
        return [
            'name'        => 'signupemail',
            'description' => 'No description provided yet...',
            'author'      => 'domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
       // return []; // Remove this line to activate

        return [
            'Domdom\Signupemail\Components\Cpsignupemail' => 'cpsignupemail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.signupemail.some_permission' => [
                'tab' => 'signupemail',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'signupemail' => [
                'label'       => 'Email Manager',
                'url'         => Backend::url('domdom/signupemail/signupemail'),
                'icon'        => 'icon-folder-open-o',
                'permissions' => ['domdom.signupemail.*'],
                'order'       => 500,

                'sideMenu' => [
                    'email' => [
                        'label' => 'Email manager',
                        'url'   => Backend::url('domdom/signupemail/signupemail'),
                        'icon'        => 'icon-folder-open-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],

                ]
            ],
        ];
    }
}

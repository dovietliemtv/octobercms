<?php namespace Domdom\Signupemail\Models;

use Model;

/**
 * signupemail Model
 */
class Signupemail extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_signupemail_signupemails';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}

<?php namespace Domdom\Signupemail\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSignupemailsTable extends Migration
{
    public function up()
    {
        Schema::create('domdom_signupemail_signupemails', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('domdom_signupemail_signupemails');
    }
}
